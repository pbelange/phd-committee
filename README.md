
# Next Meeting: Dec. 6, 2021

The next committee meeting will be hosted via zoom, on `Dec. 6, 2021 at 4 p.m.`. Here is the zoom link:


[https://cern.zoom.us/j/69945955006?pwd=dnRtWmU0ZWRzWmNlMG1JR3hWT29kUT09](https://cern.zoom.us/j/69945955006?pwd=dnRtWmU0ZWRzWmNlMG1JR3hWT29kUT09)

Meeting ID: 699 4595 5006
Passcode: 576626



# PhD Committee

